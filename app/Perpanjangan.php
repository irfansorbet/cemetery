<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perpanjangan extends Model
{
    protected $table='perpanjangan';
    protected $primaryKey='id';
}
