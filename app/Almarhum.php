<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Almarhum extends Model
{
    protected $table='almarhum';
    protected $primaryKey='id';
}
