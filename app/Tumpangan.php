<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tumpangan extends Model
{
    protected $table='tumpangan';
    protected $primaryKey='id';
}
