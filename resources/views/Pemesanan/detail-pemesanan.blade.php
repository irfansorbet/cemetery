@push('style')
    <style>
        .card-body .card {
            border: none;
        }
        .card-body .card .card-header{
            border: none;
        }
    </style>
@endpush

@extends('layouts.app') @section('content')
    <div class="">
        <div class="animated fadeIn">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Detail Pemesanan</strong>
                            <div class="pull-right">
                                <button class="btn"><i class="fa fa-print"></i> Print IPTM</button>
{{--                                <span class="badge badge-pill badge-warning">Waiting</span>--}}
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-header"><strong>Informasi Makam Tumpangan</strong></div>
                                        <div class="card-body card-block">
                                            <div class="form-group">
                                                <label for="nomor_iptm" class=" form-control-label">Nomor IPTM Lama</label>
                                                <input type="text" name="nomor_iptm" placeholder="Nomor IPTM" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_almarhum" class=" form-control-label">Nama Almarhum Lama</label>
                                                <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_pemakaman" class=" form-control-label">Pemakaman</label>
                                                <input type="text" name="nama_pemakaman" placeholder="Pemakaman" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="blok" class=" form-control-label">Blok</label>
                                                <input type="text" name="blok" placeholder="Blok" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="blad" class=" form-control-label">Blad</label>
                                                <input type="text" name="blad" placeholder="Blad" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="petak" class=" form-control-label">Petak</label>
                                                <input type="text" name="petak" placeholder="Petak" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="masa_berlaku" class=" form-control-label">Tanggal Kadaluwarsa</label>
                                                <input type="text" name="masa_berlaku" placeholder="Tanggal Kadaluwarsa" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card" style="margin-bottom: 0">
                                        <div class="card-header"><strong>Informasi Almarhum Baru</strong></div>
                                        <div class="card-body card-block">
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="nama_almarhum" class=" form-control-label">Nama Almarhum</label>
                                                    <input type="text" name="nama_almarhum" placeholder="Nama Almarhum" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="tanggal_wafat" class=" form-control-label">Tanggal Wafat</label>
                                                    <input type="text" name="tanggal_wafat" placeholder="Tanggal Wafat" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="nomor_ktp_almarhum" class=" form-control-label">No KTP Almarhum</label>
                                                    <input type="text" name="nomor_ktp_almarhum" placeholder="No KTP Almarhum" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="file_ktp_almarhum" class=" form-control-label">Foto KTP Almarhum</label>
                                                    <a href="#" target="_blank" style="display: block; padding: .375rem .75rem;">ktp_file.pdf</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="nomor_kk_almarhum" class=" form-control-label">No KK Almarhum</label>
                                                    <input type="text" name="nomor_kk_almarhum" placeholder="No KK Almarhum" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="file_kk_almarhum" class=" form-control-label">Foto KK Almarhum</label>
                                                    <a href="#" target="_blank" style="display: block; padding: .375rem .75rem;">kk_file.pdf</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header"><strong>Surat Keterangan & Lampiran</strong></div>
                                        <div class="card-body card-block">
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="file_iptm_asli" class=" form-control-label">Foto IPTM Asli</label>
                                                    <a href="#" target="_blank" style="display: block; padding: .375rem .75rem;">iptm_file.pdf</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <label for="nomor_kehilangan_kepolisian" class=" form-control-label">No SKCK</label>
                                                    <input type="text" name="nomor_kehilangan_kepolisian" placeholder="No" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="tanggal_kehilangan_kepolisian" class=" form-control-label">Tanggal</label>
                                                    <input type="text" name="tanggal_kehilangan_kepolisian" placeholder="Tanggal" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="file_sk_kehilangan_kepolisian" class=" form-control-label">Foto</label>
                                                    <a href="#" target="_blank" style="display: block; padding: .375rem .75rem;">skck_file.pdf</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <label for="nomor_sp_rtrw" class=" form-control-label">No Surat Ket RT/RW</label>
                                                    <input type="text" name="nomor_sp_rtrw" placeholder="No" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="tanggal_sp_rtrw" class=" form-control-label">Tanggal</label>
                                                    <input type="text" name="tanggal_sp_rtrw" placeholder="Tanggal" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="file_sp_rtrw" class=" form-control-label">Foto</label>
                                                    <a href="#" target="_blank" style="display: block; padding: .375rem .75rem;">skrtrw_file.pdf</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <label for="nomor_sk_kematian_rs" class=" form-control-label">No Surat Kematian</label>
                                                    <input type="text" name="nomor_sk_kematian_rs" placeholder="No" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="tanggal_sk_kematian_rs" class=" form-control-label">Tanggal</label>
                                                    <input type="text" name="tanggal_sk_kematian_rs" placeholder="Tanggal" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="file_sk_kematian_rs" class=" form-control-label">Foto</label>
                                                    <a href="#" target="_blank" style="display: block; padding: .375rem .75rem;"><i class="fa fa-image"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="card-header"><strong>Informasi Ahli Waris</strong></div>
                                        <div class="card-body card-block">
                                            <div class="form-group">
                                                <label for="nomor_ktp_ahliwaris" class=" form-control-label">No KTP Ahli Waris</label>
                                                <input type="text" name="nomor_ktp_ahliwaris" placeholder="No KTP Ahli Waris" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="nama_ahliwaris" class=" form-control-label">Nama Ahli Waris</label>
                                                <input type="text" name="nama_ahliwaris" placeholder="Nama Ahli Waris" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="telepon_ahliwaris" class=" form-control-label">Telepon Ahli Waris</label>
                                                <input type="text" name="telepon_ahliwaris" placeholder="Telepon Ahli Waris" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="hubungan_ahliwaris" class=" form-control-label">Hubungan</label>
                                                <input type="text" name="hubungan_ahliwaris" placeholder="Hubungan" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="alamat_ahliwaris" class=" form-control-label">Alamat</label>
                                                <textarea type="text" name="alamat_ahliwaris" placeholder="Alamat" class="form-control" rows="5"></textarea>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-3">
                                                    <label for="rt_ahliwaris" class=" form-control-label">RT</label>
                                                    <input type="text" name="rt_ahliwaris" placeholder="RT" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-3">
                                                    <label for="rw_ahliwaris" class=" form-control-label">RW</label>
                                                    <input type="text" name="rw_ahliwaris" placeholder="RW" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="kelurahan_ahliwaris" class=" form-control-label">Kelurahan</label>
                                                    <input type="text" name="kelurahan_ahliwaris" placeholder="Kelurahan" class="form-control">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="kecamatan_ahliwaris" class=" form-control-label">Kecamatan</label>
                                                    <input type="text" name="kecamatan_ahliwaris" placeholder="Kecamatan" class="form-control">
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="kota_administrasi" class=" form-control-label">Kota Administrasi</label>
                                                    <input type="text" name="kota_administrasi" placeholder="Kota Administrasi" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary btn-sm pull-right">
                                <i class="fa fa-dot-circle-o"></i> Approve
                            </button>
                            <button type="reset" class="btn btn-danger btn-sm pull-right mr-2">
                                <i class="fa fa-ban"></i> Reject
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
