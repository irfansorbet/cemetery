@extends('layouts.user.app')
@section('header-class')
    {{"main-header-area"}}
@endsection
@section('content')
    <style>
        * {
            box-sizing: border-box;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        /* Float four columns side by side */
        .column {
            float: left;
            width: 25%;
            padding: 0 10px;
        }

        /* Remove extra left and right margins, due to padding */
        .row {margin: 0 -5px;}

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        /* Responsive columns */
        @media screen and (max-width: 600px) {
            .column {
                width: 100%;
                display: block;
                margin-bottom: 20px;
            }
        }

        /* Style the counter cards */
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 16px;
            text-align: center;
            background-color: #f1f1f1;
        }
        .font-size{
            font-size: 20px;
            margin: 10px;
        }
    </style>
    @if(Auth::check())

        @if(count($pemakamanumum)>0)
            @php($tpu = $pemakamanumum[0])
            <div class="slider_area">
                <div class=" d-flex align-items-center "style="background-color: #2952a3;height: 100px;background-size: cover;background-repeat: no-repeat">
                    <div class="container">
                        <div class="row align-items-center justify-content-center">
                            <div class="col-lg-7 col-md-6">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class=" tab-pane container" id="information_pemakaman" role="tabpanel" aria-labelledby="information">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-information" role="tabpanel" aria-labelledby="pills-detaik-tab">
                        <div class=" tab-pane container" id="detail_pemakaman" role="tabpanel" aria-labelledby="alldetail">
                            <div class="card-header">
                                <strong>INFORMASI DETAIL {{strtoupper($tpu->nama_pemakaman)}}</strong>
                            </div>
                            <div class="card-body card-block">
                                <div class="row">
                                    <div class="col-md-4">
                                        @if($tpu->photo_pemakaman != "")
                                            <img src="/images/pemakaman/{{$tpu->photo_pemakaman}}" width="100%" alt="">
                                        @else
                                            <img src="/images/no-image-available.jpg" width="100%" alt="">
                                        @endif
                                    </div>
                                    <div class="col-md-8 ">
                                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            <div class="row form-group ">
                                                <div class="col col-md-6">
                                                    <label for="text-input" class=" form-control-label font-size">Nama Penanggung Jawab</label>
                                                </div>
                                                <div class="col col-md-6 " >
                                                    <p class="font-size">{{$tpu->fullname}} <br>NIP: {{$tpu->NIP_kepala_pemakaman}}</p>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-6">
                                                    <label class=" form-control-label font-size">Nama pemakaman</label>
                                                </div>
                                                <div class="col col-md-6 ">
                                                    <p class="font-size">{{$tpu->nama_pemakaman}}</p>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-6">
                                                    <label for="text-input" class=" form-control-label font-size">Alamat Pemakaman</label>
                                                </div>
                                                <div class="col col-md-6">
                                                    <p class="font-size">{{$tpu->alamat_pemakaman}},{{$tpu->kota_pemakaman}},{{$tpu->provinsi_pemakaman}},{{$tpu->kodepos_pemakaman}}</p>
                                                </div>

                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-6">
                                                    <label for="textearea-input " class=" form-control-label font-size">Email Pemakaman</label>
                                                </div>
                                                <div class="col col-md-6">
                                                    <p class="font-size">{{$tpu->email_pemakaman}}</p>
                                                </div>

                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-6">
                                                    <label for="textearea-input" class=" form-control-label font-size">Jumlah Pemakaman :</label>
                                                </div>
                                                <div class="col col-md-6">
                                                    <p class="font-size">{{$tpu->jumlah_pemakaman}}</p>
                                                </div>

                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-6">
                                                    <label for="text-input" class=" form-control-label font-size">Luas Pemakaman</label>
                                                </div>
                                                <div class="col col-md-6">
                                                    <p class="font-size">{{$tpu->luas_pemakaman}}</p>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-6">
                                                    <label for="text-input" class=" form-control-label font-size">Deskripsi Pemakaman</label>
                                                </div>
                                                <div class="col col-md-6">
                                                    <p class="font-size">{{$tpu->deskripsi_pemakaman}}</p>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="service_area" style="padding-bottom: 0;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section_title text-center mb-90">
                                <span class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".1s"></span>
                                <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">Pilih Jenis Perizinan</h3>
                                <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">Ajukan permohonan perpanjangan, pemindahan, atau tumpangan izin penggunaan tanah makam</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="single_service wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".5s">
                                <div class="service_icon_wrap text-center">
                                    <div class="service_icon ">
                                        <img src="/images/assets/svg_icon/service_1.png" alt="">
                                    </div>
                                </div>
                                <div class="info text-center">
                                    <h3>Perpanjangan</h3>
                                </div>
                                <div class="service_content">
                                    <ul>
                                        <li> Borrow - $350 over 3 months </li>
                                        <li> Interest rate - 292% pa fixed</li>
                                        <li>Total amount payable - $525.12</li>
                                        <li>Representative - 1,286% APR</li>
                                    </ul>
                                    <div class="apply_btn">
                                        <a href="/IPTM/perpanjangan"> <button class="boxed-btn3" >Ajukan Sekarang</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="single_service wow fadeInUp" data-wow-duration="1s" data-wow-delay=".3s">
                                <div class="service_icon_wrap text-center">
                                    <div class="service_icon ">
                                        <img src="/images/assets/svg_icon/service_2.png" alt="">
                                    </div>
                                </div>
                                <div class="info text-center">
                                    <h3>Pemindahan</h3>
                                </div>
                                <div class="service_content">
                                    <ul>
                                        <li> Borrow - $350 over 3 months </li>
                                        <li> Interest rate - 292% pa fixed</li>
                                        <li>Total amount payable - $525.12</li>
                                        <li>Representative - 1,286% APR</li>
                                    </ul>
                                    <div class="apply_btn">
                                        <a href="/IPTM/pemindahan"> <button class="boxed-btn3" >Ajukan Sekarang</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="single_service wow fadeInRight" data-wow-duration="1.2s" data-wow-delay=".5s">
                                <div class="service_icon_wrap text-center">
                                    <div class="service_icon ">
                                        <img src="/images/assets/svg_icon/service_3.png" alt="">
                                    </div>
                                </div>
                                <div class="info text-center">
                                    <h3>Tumpangan</h3>
                                </div>
                                <div class="service_content">
                                    <ul>
                                        <li> Borrow - $350 over 3 months </li>
                                        <li> Interest rate - 292% pa fixed</li>
                                        <li>Total amount payable - $525.12</li>
                                        <li>Representative - 1,286% APR</li>
                                    </ul>
                                    <div class="apply_btn">
                                        <a href="/IPTM/tumpangan"> <button class="boxed-btn3" >Ajukan Sekarang</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @else
        <h2>please Login First</h2>
    @endif
@endsection