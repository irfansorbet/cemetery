<!---->
@extends('layouts.user.app')
@section('header-class')
    {{"main-header-area"}}
@endsection
@section('content')
    <div class="slider_area">
        <div class=" d-flex align-items-center slider_bg_1"style="height: 500px;background-size: cover;background-repeat: no-repeat">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-7 col-md-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    <!-- service_area_start  -->
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section_title text-center mb-90">
                        <span class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".1s"></span>
                        <h3 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay=".2s">semua pemakaman</h3>
                    </div>
                </div>
            </div>

            @if(count($listPemakaman)>0)
                @foreach($listPemakaman as $daftarPemakaman)

                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="single_service wow fadeInLeft" data-wow-duration="1.2s" data-wow-delay=".5s">
                                <div class="service_icon_wrap text-center">
                                    <div class="service_icon ">
                                        <img src="/images/assets/svg_icon/service_1.png" alt="">
                                    </div>
                                </div>

                                <div class="info text-center">

                                    <h2>{{$daftarPemakaman->nama_pemakaman}}</h2>
                                    <h3>=================</h3>
                                </div>
                                <div class="service_content">
                                    <ul>
                                        <li> jawa tengah </li>
                                        <li> bekasi</li>
                                        <li> medan satria</li>
                                        <li> jl. pegangsaan timur no 332 blok 31 no 99 </li>
                                        <li>karet bivak@MAIL.COM </li>
                                        <li>08123412312313 </li>
                                    </ul>
                                    <div class="apply_btn" style="position: center">
                                        <a href="/pemakaman/details/{{$daftarPemakaman->id}}" class="boxed-btn3">Lihat Yuk</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </div>
    <!-- service_area_end  -->


    <!-- about_area_end  -->


@endsection